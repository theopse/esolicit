# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File if From Theopse (Self@theopse.org)
# Licensed under BSD-3-Caluse
# File:	parse.ex (esolicit/lib/parse.ex)
# Content:	HTTP
# Copyright (c) 2021 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Esolicit.Parse do
  def param([]), do: <<>>
  def param([{key, value} | list]), do: (
    Enum.reduce(list, "#{key}=#{value}", fn {key, value}, result ->
      <<result::binary, "&#{key}=#{value}"::binary>> end) end)
  def param(enum), do: enum |> Enum.to_list |> param

end
