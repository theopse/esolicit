# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File if From Theopse (Self@theopse.org)
# Licensed under BSD-3-Caluse
# File:	client.ex (esolicit/lib/client.ex)
# Content:	HTTP
# Copyright (c) 2021 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =


defmodule Esolicit.Client do

  def get(url, {:jquery, data}, type: :json), do: (
    url = Regex.replace(~r/jsoncallback=\?/, url, "jsoncallback=Esolicit.catch")
    # :random.uniform(1000000000, 9999999999)
    HTTPoison.get("#{url}&#{Esolicit.Parse.param(data)}&_=#{:math.pow(2,31) |> floor |> :random.uniform() |> rem(8999999998) |> Kernel.+(1000000000)}")
    |> case do: (
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Regex.replace(~r/^Esolicit.catch\((.*)\)$/, body, "\\1")
        |> JSON.decode
        |> case do: (
          {:ok, json} ->
            json)))
end
