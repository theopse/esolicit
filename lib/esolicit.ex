defmodule Esolicit do
  import Standard
  @moduledoc """
  Documentation for `Esolicit`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Esolicit.hello()
      :world

  """
  def hello do
    :world
  end

  def get_json(url, jquery: true), do: __MODULE__.Client.get(url, {:jquery, []}, type: :json)

  def get_json(url, data, jquery: true), do: __MODULE__.Client.get(url, {:jquery, data}, type: :json)

end
